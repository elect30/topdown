import random
import uuid
import time
from time import sleep

import pygame
from pygame import display as Display

import math
import Text

import Engine
import Properties
from Entity import Entity
from Person import Person
from Bullet import Bullet


class Arena:

    def __init__(self, x: float, y: float, width: int, height: int, engine: Engine):
        self.engine = engine
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.left_wall = x - width/2
        self.right_wall = x + width/2
        self.top_wall = y - height/2
        self.bottom_wall = y + height/2
        self.spawn_x_min = self.left_wall + Properties.Arena.SPAWN_MARGIN
        self.spawn_x_max = self.right_wall - Properties.Arena.SPAWN_MARGIN
        self.spawn_y_min = self.top_wall + Properties.Arena.SPAWN_MARGIN
        self.spawn_y_max = self.bottom_wall - Properties.Arena.SPAWN_MARGIN
        self.entities = list()
        self.players = list()
        self.bullets = list()
        self.player_attributes = list()
        self.pause_spawns = False
        self.round_end = False
        self.round_end_text = Text.Text(size=16)

    def loop(self):
        while self.engine.running:
            print(self)
            sleep(0.5)
            pass

    def spawn_player(self, x: float, y: float, a: float, genome=None):

        # Prevent spawning outside this arena. including a margin inside the walls (defined above "spawn_margin")
        while self.pause_spawns:
            sleep(0.0001)

        x = x + self.left_wall
        y = y + self.top_wall

        collide = True
        while collide:

            if x < self.spawn_x_min:
                x = self.spawn_x_min
            elif x > self.spawn_x_max:
                x = self.spawn_x_max

            if y < self.spawn_y_min:
                y = self.spawn_y_min
            elif y > self.spawn_y_max:
                y = self.spawn_y_max

            collide = False

            for player in self.players:
                if (x < player.x + Properties.Player.COLLISION_BOX_SIZE*2 and
                        x + Properties.Player.COLLISION_BOX_SIZE*2 > player.x and
                        y < player.y + Properties.Player.COLLISION_BOX_SIZE*2 and
                        Properties.Player.COLLISION_BOX_SIZE*2 + y > player.y):
                    rand_direction = random.randint(0, 360)
                    x = x + Properties.Player.COLLISION_BOX_SIZE * 2 * math.cos(math.radians(rand_direction))
                    y = y + Properties.Player.COLLISION_BOX_SIZE * 2 * math.sin(math.radians(rand_direction))
                    collide = True

        new_player = Person(x, y, a, self, genome)
        self.entities.append(new_player)
        self.players.append(new_player)

        ''' Example of how to get the genom and print it to the console '''
        '''
        genome = new_player.neural_network.genome
        count = 1
        for array in genome:
            print("\n\n count: \n" + str(count))
            print(array)
            count += 1
        '''

    def spawn_bullet(self, x: float, y: float, a: float, shooter: Entity):
        new_bullet = Bullet(x, y, a, self, shooter)
        self.entities.append(new_bullet)
        self.bullets.append(new_bullet)

    def destroy_bullet(self, destroy: Bullet):
        if not destroy.destroyed:
            self.bullets.remove(destroy)
            self.entities.remove(destroy)
            destroy.destroyed = True

    def destroy_person(self, player: Person):
        if not player.destroyed:
            self.entities.remove(player)
            self.players.remove(player)
            player.destroyed = True

        self.player_attributes.append([uuid.uuid4(),
                                       player.has_won,
                                       player.health,
                                       player.damage_dealt,
                                       player.kills,
                                       time.time() - player.time_spawned,
                                       player.neural_network.genome])

    def update_objects(self, dt: float):
        if len(self.players) <= 1:
            if len(self.players) == 1:
                self.players[0].has_won = True
                self.destroy_person(self.players[0])
            self.end_round()

        self.pause_spawns = True
        for entity in self.entities:
            entity.early_update(dt)
            entity.update(dt)
            entity.check_collisions()
            entity.check_wall_collision()
            entity.update_pos(dt)
            entity.late_update(dt)
        self.pause_spawns = False

    def end_round(self):
        self.round_end = True
        if self.engine.generation_in_progress:
            num_rounds_ended = 0
            for arena in self.engine.arenas:
                if arena.round_end:
                    num_rounds_ended += 1
            if num_rounds_ended == len(self.engine.arenas):
                self.engine.generation_in_progress = False

    def draw_arena_walls(self, display: Display):
        pygame.draw.rect(display, [0, 0, 0], [self.left_wall, self.top_wall, self.width, self.height], 1)
        if self.round_end:
            # text = self.engine.font.render("Round Over", True, [0, 0, 0])
            self.round_end_text.draw_centered(display, "Round Over", self.x, self.y)
            # display.blit(text, [self.x, self.y)

    def draw_objects(self, display: Display, draw_collision_boxes: bool):
        for entity in self.entities:
            entity.draw(display)
            if draw_collision_boxes:
                entity.draw_collision_box(display)
