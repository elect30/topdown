import math
import time
import numpy as np
import pygame

from pygame import display as Display

import Properties
from Bullet import Bullet
from Collision import CollisionBox
from Entity import Entity
from NeuralNetwork import NeuralNetwork


class Person(Entity):
    """An object that contains a neural network"""

    '''Instance attributes must be initialised in init not here (only strong typed for clarity)'''
    neural_network: NeuralNetwork
    shot_timer: float
    shoot: bool
    health: int
    has_won: bool
    damage_dealt: int
    kills: int
    time_spawned: float

    def __init__(self, x: float, y: float, a: float, arena_in, genome=None):
        super().__init__(x, y, a, arena_in)
        self.health = Properties.Player.INITIAL_HEALTH
        self.shot_timer = 0
        self.shoot = False
        self.damage_dealt = 0
        self.kills = 0
        self.has_won = False
        self.neural_network = NeuralNetwork(genome)
        self.collision_box = CollisionBox(self.x, self.y, Properties.Player.COLLISION_BOX_SIZE)
        self.time_spawned = time.time()

    def early_update(self, dt: float):
        pass

    def update(self, dt: float):

        # Neural Net Inputs ------------------------------------------------------------------
        self.shot_timer = self.shot_timer - dt
        if self.shot_timer > 0:
            reloading = True
        else:
            reloading = False

        relative_horizontal_pos = (self.x - self.arena_in.left_wall)/self.arena_in.width
        relative_vertical_pos = (self.y - self.arena_in.top_wall)/self.arena_in.height
        players_angle = self.a
        enemies = self.get_enemy_inputs()
        enemy_bullets = self.get_bullet_inputs()

        inputs = np.array([reloading, relative_horizontal_pos, relative_vertical_pos, players_angle])
        inputs = np.append(inputs, enemies)
        inputs = np.append(inputs, enemy_bullets)
        inputs = inputs.reshape(1, 52)

        # Neural Net Feed Forward ------------------------------------------------------------------

        output = self.neural_network.feed_forward(inputs)

        # Neural Net Outputs ------------------------------------------------------------------

        self.ddx = output[0][0] * Properties.Player.MAX_ACCELERATION
        self.ddy = output[0][1] * Properties.Player.MAX_ACCELERATION
        self.da = output[0][2] * Properties.Player.MAX_ROTATION_SPEED
        self.shoot = round(output[0][3])

        if self.shoot == 1 and not reloading:
            self.arena_in.spawn_bullet(self.x, self.y, self.a, self)
            self.shot_timer = Properties.Player.RELOAD_TIME

        if self.dx > Properties.Player.MAX_SPEED:
            if self.ddx > 0:
                self.ddx = 0
        elif self.dx < 0 - Properties.Player.MAX_SPEED:
            if self.ddx < 0:
                self.ddx = 0

        if self.dy > Properties.Player.MAX_SPEED:
            if self.ddy > 0:
                self.ddy = 0
        elif self.dy < 0 - Properties.Player.MAX_SPEED:
            if self.ddy < 0:
                self.ddy = 0

    def find_closest_players(self) -> list():
        relative_positions = list()
        for entity in self.arena_in.entities:
            if isinstance(entity, Person):
                if entity is not self:
                    distance = math.sqrt(math.pow(self.x - entity.x, 2) + math.pow(self.y - entity.y, 2))
                    relative_positions.append((distance, entity))
        relative_positions.sort(reverse=True)
        return [x[1] for x in relative_positions[:Properties.Player.NUMBER_OF_ENEMIES]]

    def get_enemy_inputs(self) -> list():
        closest_players = self.find_closest_players()
        entity_inputs = list()  # a list of the enemies attributes that the neural net will take in its input
        count = 0

        for player in closest_players:
            entity_inputs.append(player.x - self.x)
            entity_inputs.append(player.y - self.y)
            entity_inputs.append(player.a)
            entity_inputs.append(player.dx)
            entity_inputs.append(player.dy)
            entity_inputs.append(player.da)
            count += 1

            if count == Properties.Player.NUMBER_OF_ENEMIES:
                return entity_inputs

        if count < Properties.Player.NUMBER_OF_ENEMIES:
            for x in range(count, Properties.Player.NUMBER_OF_ENEMIES):
                entity_inputs.append(2000)
                entity_inputs.append(2000)
                entity_inputs.append(0)
                entity_inputs.append(0)
                entity_inputs.append(0)
                entity_inputs.append(0)
                count += 1
        return entity_inputs

    def find_closest_bullets(self) -> list():
        relative_positions = list()
        for entity in self.arena_in.entities:
            if isinstance(entity, Bullet):
                if entity is not self:
                    if entity.shooter is not self:  # checks to see if this bullet was shot by someone else
                        distance = math.sqrt(math.pow(self.x - entity.x, 2) + math.pow(self.y - entity.y, 2))
                        relative_positions.append((distance, entity))
        relative_positions.sort(reverse=True)
        return [x[1] for x in relative_positions[:Properties.Player.NUMBER_OF_BULLETS]]

    def get_bullet_inputs(self) -> list():
        closest_bullets = self.find_closest_bullets()
        bullet_inputs = list()  # a list of the bullets attributes that the neural net will take in its input
        count = 0

        for bullet in closest_bullets:
            bullet_inputs.append(bullet.x - self.x)
            bullet_inputs.append(bullet.y - self.y)
            bullet_inputs.append(bullet.a)
            bullet_inputs.append(bullet.dx)
            bullet_inputs.append(bullet.dy)
            count += 1

            if count == Properties.Player.NUMBER_OF_BULLETS:
                return bullet_inputs

        if count < Properties.Player.NUMBER_OF_BULLETS:
            for x in range(count, Properties.Player.NUMBER_OF_BULLETS):
                bullet_inputs.append(2000)
                bullet_inputs.append(2000)
                bullet_inputs.append(0)
                bullet_inputs.append(0)
                bullet_inputs.append(0)
                count += 1
        return bullet_inputs

    def late_update(self, dt: float):
        if self.health <= 0:
            self.health = 0
            self.arena_in.destroy_person(self)

    def collide(self, collided_with: Entity):
        if isinstance(collided_with, Bullet):
            self.health -= 1
            collided_with.shooter.damage_dealt += 1  # this probably could be done better.
            if self.health <= 0:
                self.health = 0
                collided_with.shooter.kills += 1

    def collide_left(self, collided_with: Entity):
        if isinstance(collided_with, Person):
            if self.dx < 0:
                self.dx = -self.dx * Properties.Player.PLAYER_BOUNCE_COEF

    def collide_right(self, collided_with: Entity):
        if isinstance(collided_with, Person):
            if self.dx > 0:
                self.dx = -self.dx * Properties.Player.PLAYER_BOUNCE_COEF

    def collide_top(self, collided_with: Entity):
        if isinstance(collided_with, Person):
            if self.dy < 0:
                self.dy = -self.dy * Properties.Player.PLAYER_BOUNCE_COEF

    def collide_bottom(self, collided_with: Entity):
        if isinstance(collided_with, Person):
            if self.dy > 0:
                self.dy = -self.dy * Properties.Player.PLAYER_BOUNCE_COEF

    def collide_left_wall(self):
        self.health -= 1
        if self.dx < 0:
            self.dx = -self.dx * Properties.Player.WALL_BOUNCE_COEF

    def collide_right_wall(self):
        self.health -= 1
        if self.dx > 0:
            self.dx = -self.dx * Properties.Player.WALL_BOUNCE_COEF

    def collide_top_wall(self):
        self.health -= 1
        if self.dy < 0:
            self.dy = -self.dy * Properties.Player.WALL_BOUNCE_COEF

    def collide_bottom_wall(self):
        self.health -= 1
        if self.dy > 0:
            self.dy = -self.dy * Properties.Player.WALL_BOUNCE_COEF

    def draw(self, display: Display):

        relative_vertices = [
            [Properties.Player.SIZE / 2 * math.cos(math.radians(self.a)),
             Properties.Player.SIZE / 2 * math.sin(math.radians(self.a))],
            [Properties.Player.SIZE / 2 * math.cos(math.radians(self.a + 135)),
             Properties.Player.SIZE / 2 * math.sin(math.radians(self.a + 135))],
            [Properties.Player.SIZE / 2 * math.cos(math.radians(self.a + 225)),
             Properties.Player.SIZE / 2 * math.sin(math.radians(self.a + 225))],
        ]
        actual_vertices = []
        for relative_x, relative_y in relative_vertices:
            actual_vertices.append([relative_x + self.x, relative_y + self.y])

        # actual_vertices = [[self.x - 10, self.y + 5], [self.x + 10, self.y + 5], [self.x, self.y - 10]]
        health_factor = self.health/Properties.Player.INITIAL_HEALTH
        red = int(255*(1-health_factor))
        green = int(255*health_factor)
        fill = [red, green, 0]
        pygame.draw.polygon(display, fill, actual_vertices, 0)
