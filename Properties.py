from enum import Enum


# Engine Properties
class Engine:
    FPS = 60                        # Number of frames a second                         # Updates per seconds
    TIME_SCALE = 1                 # Speed that the engine runs                        # Factor x times
    DRAW_COLLISION_BOXES = False     # Displays the collision boxes                      # Bool


# Window Properties
class Window:
    WIDTH = 1920                    # The width of the display window                   # Pixels
    HEIGHT = 1080                   # The height of the display window                  # Pixels
    FULL_SCREEN = False             # should the window be in full screen               # bool


# Arena Properties
class Arena:
    SPAWN_MARGIN = 50               # Distance from the wall where players cant spawn   # Pixels


# Bullet Properties
class Bullet:
    SPEED = 100                     # Speed of the Bullets                              # Pixels per seconds
    SIZE = 5                        # Size of the bullets                               # Pixels
    COLLISION_BOX_SIZE = 4          # Size of the bullets collision box                 # Pixels


# Player Properties
class Player:
    RELOAD_TIME = 1.5               # Time before the next shot                         # Seconds
    MAX_ACCELERATION = 50           # Maximum allowed acceleration                      # Pixels per second per second
    MAX_SPEED = 50                  # Maximum allowed speed                             # Pixels per second
    MAX_ROTATION_SPEED = 180        # Maximum rotational speed                          # Degrees per second
    WALL_BOUNCE_COEF = 0.5          # How much velocity is retained on collision        # Factor x times
    PLAYER_BOUNCE_COEF = 0.2          # How much velocity is retained on collision        # Factor x times
    SIZE = 20                       # Size of the player                                # Pixels
    COLLISION_BOX_SIZE = 18         # Size of the player collision box                  # Pixels
    INITIAL_HEALTH = 7              # Starting health of the player                     # Integer
    NUMBER_OF_ENEMIES = 3           # number of perceivable enemies                     # Integer
    NUMBER_OF_BULLETS = 6           # number of perceivable bullets                     # Integer


