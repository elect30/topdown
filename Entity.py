import pygame

import Arena
import Collision
from pygame import display
from abc import ABC, abstractmethod
from tkinter import *


from Collision import CollisionBox


class Entity(ABC):
    """An abstract class that is inherited by an object that should be classifies as an entity"""

    '''Instance attributes must be initialised in init not here (only strong typed for clarity)'''
    collision_box: CollisionBox

    '''No Static attributes'''

    def __init__(self, x: float, y: float, a: float, arena_in: Arena):
        self.x = x  # px
        self.y = y  # px
        self.a = a  # deg
        self.dx = 0
        self.dy = 0
        self.da = 0
        self.ddx = 0
        self.ddy = 0
        self.destroyed = False
        self.arena_in = arena_in

    def update_pos(self, dt: float):
        self.dx = self.dx + (self.ddx * dt)
        self.dy = self.dy + (self.ddy * dt)
        self.x = self.x + (self.dx * dt)
        self.y = self.y + (self.dy * dt)
        self.a = self.a + (self.da * dt)
        self.collision_box.update_box(self.x, self.y)

    def draw_collision_box(self, display: display):
        pygame.draw.rect(display,
                         [0, 0, 0],
                         [self.collision_box.left,
                          self.collision_box.top,
                          self.collision_box.size,
                          self.collision_box.size],
                         1)

    def check_collisions(self):
        for entity2 in self.arena_in.entities:
            if self is not entity2:
                if Collision.is_colliding(self.collision_box, entity2.collision_box):
                    self.collide(entity2)

                    if Collision.is_collide_left(self.collision_box, entity2.collision_box):
                        self.collide_left(entity2)

                    if Collision.is_collide_right(self.collision_box, entity2.collision_box):
                        self.collide_right(entity2)

                    if Collision.is_collide_top(self.collision_box, entity2.collision_box):
                        self.collide_top(entity2)

                    if Collision.is_collide_bottom(self.collision_box, entity2.collision_box):
                        self.collide_bottom(entity2)

    def check_wall_collision(self):
        if self.collision_box.left < self.arena_in.left_wall:
            self.collide_left_wall()

        if self.collision_box.right > self.arena_in.right_wall:
            self.collide_right_wall()

        if self.collision_box.top < self.arena_in.top_wall:
            self.collide_top_wall()

        if self.collision_box.bottom > self.arena_in.bottom_wall:
            self.collide_bottom_wall()

    @abstractmethod
    def early_update(self, dt: float):
        pass

    @abstractmethod
    def update(self, dt: float):
        pass

    @abstractmethod
    def late_update(self, dt: float):
        pass

    @abstractmethod
    def draw(self, canvas: Canvas):
        pass

    @abstractmethod
    def collide(self, collided_with):
        pass

    @abstractmethod
    def collide_left(self, collided_with):
        pass

    @abstractmethod
    def collide_right(self, collided_with):
        pass

    @abstractmethod
    def collide_top(self, collided_with):
        pass

    @abstractmethod
    def collide_bottom(self, collided_with):
        pass

    @abstractmethod
    def collide_left_wall(self):
        pass

    @abstractmethod
    def collide_right_wall(self):
        pass

    @abstractmethod
    def collide_top_wall(self):
        pass

    @abstractmethod
    def collide_bottom_wall(self):
        pass