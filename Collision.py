class CollisionBox:

    def __init__(self, x: float, y: float, size: float):
        self.size = size
        self.offset = size / 2
        self.bottom = y + self.offset
        self.top = y - self.offset
        self.right = x + self.offset
        self.left = x - self.offset

    def update_box(self, x: float, y: float):
        self.bottom = y + self.offset
        self.top = y - self.offset
        self.right = x + self.offset
        self.left = x - self.offset


def is_colliding(box1: CollisionBox, box2: CollisionBox) -> bool:
    return (box1.left < box2.right and box1.right > box2.left and
            box1.top < box2.bottom and
            box1.bottom > box2.top)


def is_collide_left(box1: CollisionBox, box2: CollisionBox) -> bool:
    return box2.left < box1.left < box2.right


def is_collide_right(box1: CollisionBox, box2: CollisionBox) -> bool:
    return box2.right > box1.right > box2.left


def is_collide_top(box1: CollisionBox, box2: CollisionBox) -> bool:
    return box2.top < box1.top < box2.bottom


def is_collide_bottom(box1: CollisionBox, box2: CollisionBox) -> bool:
    return box2.bottom > box1.bottom > box2.top

