import pygame


class Text:
    def __init__(self, color=(0, 0, 0), size=12):
        self.color = color
        pygame.font.init()
        self.font = pygame.font.SysFont("sans", size)

    def draw_centered(self, screen, text,  x, y):
        txt = self.font.render(text, True, self.color)
        size = self.font.size(text)
        draw_x = x - (size[0] / 2)
        draw_y = y - (size[1] / 2)
        pos = (draw_x, draw_y)
        screen.blit(txt, pos)

    def draw(self, screen, text,  x, y):
        txt = self.font.render(text, True, self.color)
        size = self.font.size(text)
        draw_x = x
        draw_y = y - (size[1] / 2)
        pos = (draw_x, draw_y)
        screen.blit(txt, pos)
