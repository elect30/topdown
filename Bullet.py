import math

import pygame
from pygame import display as Display

import Arena
from tkinter import Canvas

import Person
import Properties
from Collision import CollisionBox
from Entity import Entity


class Bullet(Entity):
    """An object that is spawned by a Person object and caused damage and is destroyed on collision"""

    '''Instance attributes must be initialised in init not here (only strong typed for clarity)'''
    shooter: Person

    def __init__(self, x: float, y: float, a: float, arena_in: Arena, shooter: Entity):
        super().__init__(x, y, a, arena_in)
        self.dx = math.cos(math.radians(a)) * Properties.Bullet.SPEED
        self.dy = math.sin(math.radians(a)) * Properties.Bullet.SPEED
        self.x = self.x + (math.cos(math.radians(a)) * 18)
        self.y = self.y + (math.sin(math.radians(a)) * 18)
        self.shooter = shooter
        self.collision_box = CollisionBox(self.x, self.y, Properties.Bullet.COLLISION_BOX_SIZE)

    def early_update(self, dt: float):
        pass

    def update(self, dt: float):
        pass

    def collide(self, collided_with):
        if not self.destroyed:
            self.arena_in.destroy_bullet(self)

    def collide_left(self, collided_with: Entity):
        pass

    def collide_right(self, collided_with: Entity):
        pass

    def collide_top(self, collided_with: Entity):
        pass

    def collide_bottom(self, collided_with: Entity):
        pass

    def collide_left_wall(self):
        self.arena_in.destroy_bullet(self)

    def collide_right_wall(self):
        self.arena_in.destroy_bullet(self)

    def collide_top_wall(self):
        self.arena_in.destroy_bullet(self)

    def collide_bottom_wall(self):
        self.arena_in.destroy_bullet(self)

    def draw(self, display: Display):

        pygame.draw.circle(display, [0, 0, 0], [int(self.x), int(self.y)], int(Properties.Bullet.SIZE/2))

        # canvas.create_oval(self.x - Properties.Bullet.SIZE / 2,
        #                    self.y - Properties.Bullet.SIZE / 2,
        #                    self.x + Properties.Bullet.SIZE / 2,
        #                    self.y + Properties.Bullet.SIZE / 2)

    def late_update(self, dt: float):
        pass
