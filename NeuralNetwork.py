import math

import numpy as np

input_min: np.array = None
input_max: np.array = None


class NeuralNetwork(object):

    def __init__(self, genome=None):
        self.num_layers = 4
        self.num_inputs = 52
        self.h1_width = 5
        self.h2_width = 5
        self.num_outputs = 4

        global input_min
        if input_min is None:
            input_min = np.ones(self.num_inputs) * math.inf

        global input_max
        if input_max is None:
            input_max = np.zeros(self.num_inputs)

        if genome is None:
            self.genome = self.gen_genome()
        else:
            self.genome = genome

    def gen_genome(self):
        """initiate random genome if none provided"""
        # genome is an np.array of 3 matrices - thetas.
        # each theta holds the weights (and biases) of a h layer.
        # Each row of a theta corresponds to a neuron.
        # Each column of theta is a different input. thetas to be transposed before dotting with layers.
        theta1 = np.random.uniform(low=-1, high=1, size=(self.h1_width, self.num_inputs + 1))
        theta2 = np.random.uniform(low=-1, high=1, size=(self.h2_width, self.h1_width + 1))
        theta3 = np.random.uniform(low=-1, high=1, size=(self.num_outputs, self.h2_width + 1))
        return [theta1, theta2, theta3]

    def feed_forward(self, inputs: np.array) -> np.array:
        """Returns the output of the network, based on the genome and inputs"""
        """input has to be a 1 by 14 np.array. Also make sure input is already normalised - helps with optimisation."""

        '''
        global input_min
        input_min = np.minimum(input_min, inputs)

        global input_max
        input_max = np.maximum(input_max, inputs)

        inputs = inputs / (input_max - input_min)
        '''

        '''extract each theta from the genome from this instance'''
        theta1 = self.genome[0]
        theta2 = self.genome[1]
        theta3 = self.genome[2]

        # The feedforward algorithm
        h1 = self.sigmoid(np.dot(self.add_ones(inputs), theta1.T))
        h2 = self.sigmoid(np.dot(self.add_ones(h1), theta2.T))
        output = self.sigmoid(np.dot(self.add_ones(h2), theta3.T))

        # modify outputs
        output[0][0] = (output[0][0] - 0.5) * 2
        output[0][1] = (output[0][1] - 0.5) * 2
        output[0][2] = (output[0][2] - 0.5) * 2

        """outputs are dx, dy, da, shoot"""
        return output

    @staticmethod
    def add_ones(x: np.array):
        """ appends column of ones to theta matrices. """
        m = x.shape[0]
        ones = np.ones((m, 1), dtype=int)
        output = np.append(ones, x, axis=1)
        return output

    @staticmethod
    def sigmoid(z):
        """The sigmoid function."""
        return 1.0 / (1.0 + np.exp(-z))

    '''
        def get_weights(self, layer, neuroninput, neuronoutput):
            return self.weights[layer][neuronoutput][neuroninput]

        def set_weights(self, layer, neuroninput, neuronoutput, value):
            self.weights[layer][neuronoutput][neuroninput] = value

        def get_biases(self, layer, neuronoutput):
            return self.biases[layer][neuronoutput]

        def set_biases(self, layer, neuronoutput, value):
            self.biases[layer][neuronoutput] = value

        def to_genes(self):
            genes = []
            for x in range(0, (self.num_layers - 1)):
                for y in range(0, (self.sizes[x + 1])):
                    genes.extend(self.weights[x][y])
            for x in range(len(self.biases)):
                for element in self.biases[x]:
                    genes.extend(element)
            return genes

        def from_genes(self, genes):
            weights1D = genes[:-self.numberOfBiases]
            biases1D = genes[self.numberOfWeights:]
            i = 0
            weights = []
            for x in range(0, (self.num_layers - 1)):
                layer = np.zeros((self.sizes[x + 1], self.sizes[x]), dtype=np.float64)
                for y in range(0, (self.sizes[x + 1])):
                    for z in range(0, self.sizes[x]):
                        weight = weights1D[i]
                        layer[y, z] = weight
                        i += 1
                weights.append(layer)

            """ Now we parse the biases """
            biases = []
            i = 0
            for x in range(1, (self.num_layers)):
                biase = np.zeros((self.sizes[x], 1), dtype=np.float64)
                for y in range(0, (self.sizes[x])):
                    biase[y, 0] = biases1D[i]
                    i += 1
                biases.append(biase)

            self.weights = weights
            self.biases = biases

        def save(self, filename):
            """Save the neural network to the file filename """

            data = {"sizes": self.sizes,
                    "weights": [w.tolist() for w in self.weights],
                    "biases": [b.tolist() for b in self.biases]}
            dir = os.path.dirname(filename)
            if not os.path.exists(dir):
                os.makedirs(dir)
            f = open(filename, "wb")
            json.dump(data, f)
            f.close()
    '''


'''
#### Loading a Network
def load(filename):
    """Load a neural network from the file ``filename``.  Returns an
    instance of Network.
    """
    f = open(filename, "r")
    data = json.load(f)
    f.close()
    net = Network(data["sizes"])
    net.weights = [np.array(w) for w in data["weights"]]
    net.biases = [np.array(b) for b in data["biases"]]
    return net
'''
