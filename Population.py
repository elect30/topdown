import random
from threading import Thread
import numpy as np
import pandas as pd
import time

class Population(Thread):
    players = list()
    generation = 0

    def __init__(self, engine):
        Thread.__init__(self)
        self.engine = engine

    def first_round(self):
        # spawn 64 players 2 per arena, 32 arenas
        for arena in self.engine.arenas:
            for x in range(2):
                arena.spawn_player(random.random() * arena.width, random.random() * arena.height, random.random() * 360)
                arena.round_end = False

        self.generation = 1
        self.engine.start_generation()

        while self.engine.generation_in_progress and self.engine.running:
            time.sleep(0.5)
            pass

    def next_generation(self):
        # After 32 victors emerge, get Player attributes as list of list
        player_attributes = list()
        arena_index = 1
        for arena in self.engine.arenas:
            for i in arena.player_attributes:
                temp_list = [arena_index] + i
                player_attributes.append(temp_list)
            arena_index += 1

        # wait for victors to finish #  TODO: handle draws

        # player attributes are structured into a generational data frame
        # arena_index, bug_id, winner, HP_left, HP_dealt, time_survived, kills, genome
        gen_df = pd.DataFrame.from_records(player_attributes,
                                           columns=['arena_index', 'bug_id', 'winner',
                                                    'HP_left', 'HP_dealt', 'time_survived', 'kills', 'genome'])
        # calculate and append fitness of all players
        gen_df = self.append_fitness(gen_df)

        # calculate breeding probability and append for all players
        gen_df = self.append_breeding_prob(gen_df)

        # next generation of 64 bugs
        children = []

        for i in range(64):
            # pick breeding pair, from pool of 64
            breeding_pair = np.random.choice(gen_df.loc[:, 'genome'], 2, replace=False,
                                             p=gen_df.loc[:, 'breeding_prob'])

            # cross their genomes to make child
            child = self.cross_over1(breeding_pair[0], breeding_pair[1])

            # mutate child genome
            child = self.mutate_genome(child)

            children.append(child)

        i = 0
        for arena in self.engine.arenas:
            for x in range(2):
                arena.spawn_player(random.random() * arena.width,
                                   random.random() * arena.height,
                                   random.random() * 360,
                                   children[i])
                i += 1

        self.generation += 1
        self.engine.start_generation()

    def run(self):
        self.first_round()

        while self.engine.running:
            self.next_generation()
            while self.engine.generation_in_progress and self.engine.running:
                time.sleep(0.5)
                pass

    @staticmethod
    def fitness_map(time, T1=10, F1=1, T2=30, F2=3, H=4):
        #Funct crosses two points (T1,F1) and (T2, F2)

        #calculate stretch (width) coefficient of sigmoid funct
        w=(T1-T2)/(np.log((H-F2)/F2)-np.log((H-F1)/F1))
        #calculate shift coefficient of sigmoid funct
        c=T1 + w*np.log((H-F1)/F1)

        #calc fitness as a logistic function mapped between 0-H, currently H is 4
        fitness =H/(1+np.exp(-(time-c)/w))
        return fitness

    @staticmethod
    def append_fitness(df):
        df['fitness'] = df['HP_left'] + df['HP_dealt'] + Population.fitness_map(df['time_survived'])
        return df


    @staticmethod
    def append_breeding_prob(df):
        nominator = df['fitness'].sum()
        df['breeding_prob'] = df['fitness'] / nominator
        return df

    @staticmethod
    def cross_over1(genome_dad, genome_mum) -> np.array:
        # Randomly picks weights from each parents genome matrixes, creates one child

        size1 = genome_mum[0].shape
        size2 = genome_mum[1].shape
        size3 = genome_mum[2].shape

        mask1 = np.random.choice([0, 1], size=size1, p=[0.5, 0.5])
        mask2 = np.random.choice([0, 1], size=size2, p=[0.5, 0.5])
        mask3 = np.random.choice([0, 1], size=size3, p=[0.5, 0.5])

        theta1 = np.multiply(mask1, genome_dad[0]) + np.multiply(np.ones(size1) - mask1, genome_mum[0])
        theta2 = np.multiply(mask2, genome_dad[1]) + np.multiply(np.ones(size2) - mask2, genome_mum[1])
        theta3 = np.multiply(mask3, genome_dad[2]) + np.multiply(np.ones(size3) - mask3, genome_mum[2])

        # returns the child's genome
        return [theta1, theta2, theta3]

    def cross_over2(self, genome1, genome2) -> np.array:
        pass

    def cross_over3(self, genome1, genome2) -> np.array:
        pass

    @staticmethod
    def mutate_genome(genome):
        # After cross over of each genome, the bug's beta coefficients each have a 20% chance of mutating.
        # If a mutation occurs on a weight, use the normal distribution to update, beta = beta + beta*z, z~N(mu=0,var=1)
        theta1 = genome[0]
        theta2 = genome[1]
        theta3 = genome[2]

        size1 = theta1.shape
        size2 = theta2.shape
        size3 = theta3.shape

        m1 = np.random.normal(0, 1, size1)
        m2 = np.random.normal(0, 1, size2)
        m3 = np.random.normal(0, 1, size3)

        mask1 = np.random.choice([1,0], size=size1, p=[0.05, 0.8])
        mask2 = np.random.choice([1,0], size=size2, p=[0.05, 0.8])
        mask3 = np.random.choice([1,0], size=size3, p=[0.05, 0.8])

        theta1 = theta1 + np.multiply(mask1, m1))
        theta2 = theta2 + np.multiply(mask2, m2))
        theta3 = theta3 + np.multiply(mask3, m3))

        return [theta1, theta2, theta3]

    ####################################################################################################
