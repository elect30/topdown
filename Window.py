from tkinter import Frame, Canvas

import Properties


class Window(Frame):

    canvas: Canvas

    def __init__(self):
        print("Start Engine")
        Frame.__init__(self)
        self.master.title("Adversarial Genetic Neural Network For a Top-down Shooter")
        self.master.resizable(False, False)
        self.grid()

        frame = Frame(self)
        frame.grid()

        if Properties.Window.FULL_SCREEN:
            self.width = frame.master.winfo_screenwidth()
            self.height = frame.master.winfo_screenheight()
            self.master.attributes("-fullscreen", True)

        self.canvas = Canvas(frame, width=Properties.Window.WIDTH, height=Properties.Window.HEIGHT, bg="white")
        self.canvas.grid(columnspan=3)
        self.canvas.focus_set()

    def draw(self):
        self.update()
