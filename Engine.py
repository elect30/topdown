import time
import Arena
import Properties
import pygame
from pygame import display, font
from Population import Population
from Text import Text


class Engine:
    running = False  # bool
    arenas = list()
    threads = list()
    population: Population
    display: display
    generation_in_progress = False

    def __init__(self):
        pygame.init()
        self.display = display.set_mode((Properties.Window.WIDTH, Properties.Window.HEIGHT))
        pygame.display.set_caption("Instruction Screen")
        self.font = font.Font(None, 12)
        self.gui_text = Text(size=16)
        num_rows = 4
        num_col = 8
        arena_width = Properties.Window.WIDTH / num_col
        arena_height = Properties.Window.HEIGHT / num_rows

        for x in range(num_rows):
            for y in range(num_col):
                arena = Arena.Arena(arena_width / 2 + (arena_width * y),
                                arena_height / 2 + (arena_height * x),
                                arena_width,
                                arena_height, self)
                self.arenas.append(arena)

        self.population = Population(self)
        self.population.start()
        self.FPS_check = 0
        self.frame_counter = 0
        self.frame_count = 0
        self.running = True
        self.clock()

    def clock(self):
        target_time = 1 / Properties.Engine.FPS
        last_time = float(time.time())
        while self.running:
            start_time = float(time.time())
            dt = (start_time - last_time) * Properties.Engine.TIME_SCALE
            # print(str(round(1/(dt+0.00000000000001))))
            last_time = time.time()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            self.display.fill([255, 255, 255])

            for arena in self.arenas:
                if self.generation_in_progress:
                    arena.update_objects(dt)
                arena.draw_objects(self.display, Properties.Engine.DRAW_COLLISION_BOXES)
                arena.draw_arena_walls(self.display)
            self.update_FPS()
            self.gui_text.draw(self.display, "FPS: " + str(self.frame_count), 5, 30)
            self.gui_text.draw(self.display, "Generation: " + str(self.population.generation), 5, 10)
            pygame.display.flip()
            while time.time() < start_time + target_time:
                pass

    def start_generation(self):
        for arena in self.arenas:
            arena.round_end = False
            arena.player_attributes = list()
        self.generation_in_progress = True

    def update_FPS(self):
        if time.time() < self.FPS_check + 1:
            self.frame_counter += 1
        else:
            self.frame_count = self.frame_counter
            self.frame_counter = 0
            self.FPS_check = time.time()



if __name__ == '__main__':
    Engine()
